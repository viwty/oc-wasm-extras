//! Multiplexes screens using a singular gpu, creating a single big 'screen'

use std::ops::{Deref, DerefMut};

use oc_wasm_futures::invoke::Buffer;
use oc_wasm_opencomputers::common::{Dimension, Vector2};
use oc_wasm_opencomputers::error::Error;
use oc_wasm_opencomputers::gpu::CharacterCellContents;
use oc_wasm_opencomputers::gpu::{Gpu, TextDirection};
use oc_wasm_opencomputers::{
    common::{Lockable, Point},
    gpu,
};
use oc_wasm_safe::{component::Invoker, Address};

/// A hologram component.
#[derive(Clone, Debug, Eq, Hash, PartialEq, PartialOrd)]
pub struct Sewer {
    gpu: Gpu,
    screens: Vec<Vec<Address>>,
}

impl Sewer {
    /// Creates a wrapper around a hologram.
    ///
    /// The `address` parameter is the address of the hologram. It is not checked for correctness at
    /// this time because network topology could change after this function returns; as such, each
    /// usage of the value may fail instead.
    #[must_use = "This function is only useful for its return value"]
    pub fn new(gpu: Gpu, screens: impl IntoIterator<Item = Address>, width: usize) -> Self {
        let v: Vec<Address> = screens.into_iter().collect();
        Self {
            gpu,
            screens: v
                .chunks(width)
                .map(|c| c.iter().map(|s| *s).collect())
                .collect(),
        }
    }
}

impl<'a, B: 'a + Buffer> Lockable<'a, 'a, B> for Sewer {
    type Locked = Locked<'a, B>;

    fn lock(&self, invoker: &'a mut Invoker, buffer: &'a mut B) -> Self::Locked {
        Locked {
            gpu: self.gpu.lock(invoker, buffer),
            screens: self.screens.clone(),
            current_screen: self.screens[0][0],
        }
    }
}

pub struct Locked<'a, B: Buffer> {
    gpu: gpu::Locked<'a, B>,
    screens: Vec<Vec<Address>>,
    current_screen: Address,
}

impl<'a, B: Buffer> Deref for Locked<'a, B> {
    type Target = <Gpu as Lockable<'a, 'a, B>>::Locked;

    fn deref(&self) -> &Self::Target {
        &self.gpu
    }
}
impl<'a, B: Buffer> DerefMut for Locked<'a, B> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.gpu
    }
}

fn calc_optimal_size(resolution: Dimension) -> Dimension {
    // Max t3 screen size.
    let ar = 8.0 / 6.0;
    if ar == 1.5 {
        return resolution;
    }

    let mut height = resolution.height as f32;
    let mut width = ar * 2.0 * height;

    if width > resolution.width as f32 {
        let rat = width / resolution.width as f32;
        height *= rat.recip();
        width = ar * 2.0 * height;
    }

    Dimension {
        width: width as u32,
        height: height as u32,
    }
}

impl<'a, B: Buffer> Locked<'a, B> {
    pub async fn init(&mut self) -> Result<(), Error> {
        let screen = self.screens[0][0];
        self.gpu.bind(screen, false).await?;
        let res = calc_optimal_size(self.gpu.max_resolution().await?);

        for row in self.screens.iter() {
            for screen in row.iter() {
                self.gpu.bind(*screen, true).await?;
                self.gpu.set_resolution(res).await?;
                self.gpu.fill(Point { x: 1, y: 1 }, res, ' ').await?;
            }
        }

        self.gpu.bind(screen, false).await?;
        self.current_screen = screen;

        let buffer = self.gpu.allocate_buffer(Some(res)).await?;
        self.gpu.set_active_buffer(buffer).await?;

        Ok(())
    }

    /// Returns the maximum supported resolution based on the tiers of the GPU and the bound
    /// screen.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`BadScreen`](Error::BadScreen)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn max_resolution(&mut self) -> Result<Dimension, Error> {
        // We assume all the screens are the same tier.
        let mut res = self.gpu.max_resolution().await?;
        res.width *= self.screens.len() as u32;
        // If this fails... idk maybe pass it a vector with values?
        res.height *= self.screens[0].len() as u32;

        Ok(res)
    }

    /// Returns the resolution currently in use.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`BadScreen`](Error::BadScreen)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn get_resolution(&mut self) -> Result<Dimension, Error> {
        // We assume all the screens are the same tier.
        let mut res = self.gpu.get_resolution().await?;
        res.height *= self.screens.len() as u32;
        // If this fails... idk maybe pass it a vector with values?
        res.width *= self.screens[0].len() as u32;

        Ok(res)
    }

    /// Sets the screen resolution, returning whether or not it was changed from its previous
    /// value.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`BadCoordinate`](Error::BadCoordinate)
    /// * [`BadScreen`](Error::BadScreen)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn set_resolution(&mut self, _resolution: Dimension) -> Result<bool, Error> {
        todo!()
    }

    /// Returns the viewport currently in use.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`BadScreen`](Error::BadScreen)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn get_viewport(&mut self) -> Result<Dimension, Error> {
        todo!()
    }

    /// Sets the viewport, returning whether or not it was changed from its previous value.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`BadCoordinate`](Error::BadCoordinate)
    /// * [`BadScreen`](Error::BadScreen)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn set_viewport(&mut self, _resolution: Dimension) -> Result<bool, Error> {
        todo!()
    }

    /// Returns the character and colours at a specific character cell.
    ///
    /// It is possible for a Java string to contain an unpaired UTF-16 surrogate half. It is
    /// possible (in Lua, at least) to place such a byte sequence into a character cell in a
    /// screen. Should this method be called on such a character cell, a replacement character is
    /// silently returned instead.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`BadCoordinate`](Error::BadCoordinate)
    /// * [`BadScreen`](Error::BadScreen)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn get(&mut self, point: Point) -> Result<CharacterCellContents, Error> {
        let (screen, point) = self.get_affected(point).await?;
        if screen != self.current_screen {
            self.gpu.bind(screen, false).await?;
        }

        self.gpu.get(point).await
    }

    /// Writes text to the screen.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`BadScreen`](Error::BadScreen)
    /// * [`NotEnoughEnergy`](Error::NotEnoughEnergy)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn set(
        &mut self,
        position: Point,
        text: &str,
        direction: TextDirection,
    ) -> Result<(), Error> {
        let (screen, point) = self.get_affected(position).await?;
        self.bind(screen).await?;

        self.gpu.set(point, text, direction).await
    }

    /// Copies data from one rectangle to another.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`BadScreen`](Error::BadScreen)
    /// * [`NotEnoughEnergy`](Error::NotEnoughEnergy)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn copy(
        &mut self,
        _source: Point,
        _dimension: Dimension,
        _translation: Vector2,
    ) -> Result<(), Error> {
        // I do not need this, but implementing it should be easy.
        todo!()
    }

    /// Fills a rectangle with a character.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`BadScreen`](Error::BadScreen)
    /// * [`NotEnoughEnergy`](Error::NotEnoughEnergy)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn fill(
        &mut self,
        target: Point,
        dimension: Dimension,
        character: char,
    ) -> Result<(), Error> {
        let (screen, point) = self.get_affected(target).await?;
        self.bind(screen).await?;

        self.gpu.fill(point, dimension, character).await
    }

    /// Given a point, returns the affected screen and a new point.
    // TODO: Multi-screen operations.
    async fn get_affected(&mut self, p: Point) -> Result<(Address, Point), Error> {
        let res = self.gpu.get_resolution().await?;

        let x = (p.x - 1) / res.width;
        let y = (p.y - 1) / res.height;

        let screen = self
            .screens
            .get(y as usize)
            .map(|screens| screens.get(x as usize))
            .flatten()
            .ok_or(Error::BadScreen)?;

        Ok((
            *screen,
            Point {
                x: p.x - res.width * x,
                y: p.y - res.height * y,
            },
        ))
    }

    /// Flush the content of the current buffer to the current screen.
    pub async fn flush(&mut self) -> Result<(), Error> {
        self.gpu.bitblt(None, None, None, None, None).await?;
        Ok(())
    }

    /// Returns a list of iterators of positions of every screen to aid with buffering.
    pub async fn screens(&mut self) -> Result<Vec<((usize, usize), (usize, usize))>, Error> {
        let res = self.gpu.get_resolution().await?;
        let height = self.screens.len();
        let width = self.screens[0].len();
        let mut out = vec![];

        let w = res.width as usize;
        let h = res.height as usize;
        for y in 1..=height {
            for x in 1..=width {
                out.push(((w * (x - 1) + 1, w * x), (h * (y - 1) + 1, h * y)))
            }
        }

        Ok(out)
    }

    /// Checks if we should bind to the screen, and does so if we do.
    async fn bind(&mut self, screen: Address) -> Result<(), Error> {
        if self.current_screen != screen {
            self.gpu.bitblt(Some(0), None, None, Some(1), None).await?;
            self.gpu.bind(screen, false).await?;
            self.current_screen = screen;
            self.gpu.bitblt(Some(1), None, None, Some(0), None).await?;
        }

        Ok(())
    }
}
