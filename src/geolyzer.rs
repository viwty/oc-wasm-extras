//! Provides high-level access to the geolyzer APIs.

use oc_wasm_futures::invoke::{component_method, Buffer};
use oc_wasm_helpers::Lockable;
use oc_wasm_safe::{component::Invoker, error::Error, Address};

/// The type name for geolyzer components.
pub const TYPE: &str = "geolyzer";

/// A geolyzer component.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Geolyzer(Address);

impl Geolyzer {
    /// Creates a wrapper around a geolyzer.
    ///
    /// The `address` parameter is the address of the geolyzer. It is not checked for correctness at
    /// this time because network topology could change after this function returns; as such, each
    /// usage of the value may fail instead.
    #[must_use = "This function is only useful for its return value"]
    pub fn new(address: Address) -> Self {
        Self(address)
    }

    /// Returns the address of the geolyzer.
    #[must_use = "This function is only useful for its return value"]
    pub fn address(&self) -> &Address {
        &self.0
    }
}

impl<'a, B: 'a + Buffer> Lockable<'a, 'a, B> for Geolyzer {
    type Locked = Locked<'a, B>;

    fn lock(&self, invoker: &'a mut Invoker, buffer: &'a mut B) -> Self::Locked {
        Locked {
            address: self.0,
            invoker,
            buffer,
        }
    }
}

/// A hologram component on which methods can be invoked.
///
/// This type combines a hologram address, an [`Invoker`] that can be used to make method calls, and
/// a scratch buffer used to perform CBOR encoding and decoding. A value of this type can be
/// created by calling [`Hologram::lock`], and it can be dropped to return the borrow of the invoker
/// and buffer to the caller so they can be reused for other purposes.
///
/// The `'a` lifetime is the lifetime of the invoker and the buffer. The `B` type is the type of
/// scratch buffer to use.
pub struct Locked<'a, B: Buffer> {
    /// The component address.
    address: Address,

    /// The invoker.
    invoker: &'a mut Invoker,

    /// The buffer.
    buffer: &'a mut B,
}

impl<'a, B: Buffer> Locked<'a, B> {
    /// Analyzes the density of an area at the specified relative coordinates x, z and y.
    /// This will return a list of hardness values for the blocks in the specified range.
    /// The coordinates are relative to the location of the geolyzer.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn scan(
        &mut self,
        x: i32,
        z: i32,
        y: i32,
        w: i32,
        d: i32,
        h: i32,
    ) -> Result<Vec<f32>, Error> {
        let ret: (Vec<f32>,) = component_method(
            self.invoker,
            self.buffer,
            &self.address,
            "scan",
            Some(&(x, z, y, w, d, h)),
        )
        .await?;
        Ok(ret.0)
    }
}
