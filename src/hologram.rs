//! Provides high-level access to the hologram APIs.

use minicbor::encode;
use oc_wasm_futures::invoke::{component_method, Buffer};
use oc_wasm_helpers::Lockable;
use oc_wasm_safe::{component::Invoker, error::Error, extref, Address};

/// The type name for hologram components.
pub const TYPE: &str = "hologram";

/// A hologram component.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Hologram(Address);

impl Hologram {
    /// Creates a wrapper around a hologram.
    ///
    /// The `address` parameter is the address of the hologram. It is not checked for correctness at
    /// this time because network topology could change after this function returns; as such, each
    /// usage of the value may fail instead.
    #[must_use = "This function is only useful for its return value"]
    pub fn new(address: Address) -> Self {
        Self(address)
    }

    /// Returns the address of the hologram.
    #[must_use = "This function is only useful for its return value"]
    pub fn address(&self) -> &Address {
        &self.0
    }
}

impl<'a, B: 'a + Buffer> Lockable<'a, 'a, B> for Hologram {
    type Locked = Locked<'a, B>;

    fn lock(&self, invoker: &'a mut Invoker, buffer: &'a mut B) -> Self::Locked {
        Locked {
            address: self.0,
            invoker,
            buffer,
        }
    }
}

/// A hologram component on which methods can be invoked.
///
/// This type combines a hologram address, an [`Invoker`] that can be used to make method calls, and
/// a scratch buffer used to perform CBOR encoding and decoding. A value of this type can be
/// created by calling [`Hologram::lock`], and it can be dropped to return the borrow of the invoker
/// and buffer to the caller so they can be reused for other purposes.
///
/// The `'a` lifetime is the lifetime of the invoker and the buffer. The `B` type is the type of
/// scratch buffer to use.
pub struct Locked<'a, B: Buffer> {
    /// The component address.
    address: Address,

    /// The invoker.
    invoker: &'a mut Invoker,

    /// The buffer.
    buffer: &'a mut B,
}

impl<'a, B: Buffer> Locked<'a, B> {
    /// Sets the relative render projection offsets of the hologram.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn set_translation(&mut self, tx: i32, ty: i32, tz: i32) -> Result<(), Error> {
        component_method(
            self.invoker,
            self.buffer,
            &self.address,
            "setTranslation",
            Some(&(tx, ty, tz)),
        )
        .await?;
        Ok(())
    }

    /// Fills an interval of a column with the specified value.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn fill(
        &mut self,
        x: i32,
        z: i32,
        min_y: Option<i32>,
        max_y: i32,
        value: impl HoloVal,
    ) -> Result<(), Error> {
        component_method::<_, _, _>(
            self.invoker,
            self.buffer,
            &self.address,
            "fill",
            Some(&(x, z, min_y.unwrap_or(0), max_y, value.get())),
        )
        .await?;
        Ok(())
    }

    /// Set the value for the specified voxel.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn set(&mut self, x: i32, y: i32, z: i32, value: impl HoloVal) -> Result<(), Error> {
        match component_method::<_, (), _>(
            self.invoker,
            self.buffer,
            &self.address,
            "set",
            Some(&(x, y, z, value.get())),
        )
        .await
        {
            Ok(_) => Ok(()),
            Err(oc_wasm_safe::component::MethodCallError::CborDecode) => Ok(()),
            Err(e) => Err(e.into()),
        }
    }

    /// Get the color depth supported by the hologram.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn max_depth(&mut self) -> Result<i32, Error> {
        let ret: (i32,) = component_method::<(), _, _>(
            self.invoker,
            self.buffer,
            &self.address,
            "maxDepth",
            None,
        )
        .await?;
        Ok(ret.0)
    }

    /// Get the color defined for the specific value.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn get_palette_color(&mut self, index: i32) -> Result<u32, Error> {
        let ret: (u32,) = component_method(
            self.invoker,
            self.buffer,
            &self.address,
            "getPaletteColor",
            Some(&(index,)),
        )
        .await?;
        Ok(ret.0)
    }

    /// Copies an area of columns by the specified translation.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn copy(
        &mut self,
        x: i32,
        z: i32,
        sx: i32,
        sz: i32,
        tx: i32,
        tz: i32,
    ) -> Result<(), Error> {
        component_method::<_, _, _>(
            self.invoker,
            self.buffer,
            &self.address,
            "copy",
            Some(&(x, z, sx, sz, tx, tz)),
        )
        .await?;
        Ok(())
    }

    /// Set the render scale. A larger scale consumes more energy.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn set_scale(&mut self, scale: f32) -> Result<(), Error> {
        component_method::<_, _, _>(
            self.invoker,
            self.buffer,
            &self.address,
            "setScale",
            Some(&(scale,)),
        )
        .await?;
        Ok(())
    }

    /// Returns the render scale of the hologram.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn get_scale(&mut self) -> Result<f32, Error> {
        let ret: (f32,) = component_method::<(), _, _>(
            self.invoker,
            self.buffer,
            &self.address,
            "getScale",
            None,
        )
        .await?;
        Ok(ret.0)
    }

    /// Set the base rotation of the displayed hologram.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn set_rotation(&mut self, angle: i32, x: i32, y: i32, z: i32) -> Result<(), Error> {
        component_method::<_, _, _>(
            self.invoker,
            self.buffer,
            &self.address,
            "setRotation",
            Some(&(angle, x, y, z)),
        )
        .await?;
        Ok(())
    }

    /// Returns the value for the specified voxel.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn get(&mut self, x: i32, y: i32, z: i32) -> Result<u8, Error> {
        let ret: (u8,) = component_method::<_, _, _>(
            self.invoker,
            self.buffer,
            &self.address,
            "get",
            Some(&(x, y, z)),
        )
        .await?;
        Ok(ret.0)
    }

    /// Get the dimension of the x, y, z axes.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn get_dimensions(&mut self) -> Result<(i32, i32, i32), Error> {
        let ret: (i32, i32, i32) = component_method::<(), _, _>(
            self.invoker,
            self.buffer,
            &self.address,
            "getDimensions",
            None,
        )
        .await?;
        Ok(ret)
    }

    /// Returns the relative render projection offsets of the hologram.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn get_translation(&mut self) -> Result<(f32, f32, f32), Error> {
        let ret: (f32, f32, f32) = component_method::<(), _, _>(
            self.invoker,
            self.buffer,
            &self.address,
            "getTranslation",
            None,
        )
        .await?;
        Ok(ret)
    }

    /// Set the rotation speed of the displayed hologram.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn set_rotation_speed(
        &mut self,
        angle: i32,
        x: i32,
        y: i32,
        z: i32,
    ) -> Result<(), Error> {
        component_method::<_, (bool,), _>(
            self.invoker,
            self.buffer,
            &self.address,
            "setRotationSpeed",
            Some(&(angle, x, y, z)),
        )
        .await?;
        Ok(())
    }

    /// Set the color defined for the specified value.
    /// Returns the previous color.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn set_palette_color(&mut self, index: i32, value: u32) -> Result<u32, Error> {
        let ret: (u32,) = component_method::<_, _, _>(
            self.invoker,
            self.buffer,
            &self.address,
            "setPaletteColor",
            Some(&(index, value)),
        )
        .await?;
        Ok(ret.0)
    }

    /// Clears the hologram.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn clear(&mut self) -> Result<(), Error> {
        component_method::<(), _, _>(self.invoker, self.buffer, &self.address, "clear", None)
            .await?;
        Ok(())
    }

    /// Set the buffer to a voxel byte array where each byte represents a color, ordering is x, z, y.
    ///
    /// # Errors
    /// * [`BadComponent`](Error::BadComponent)
    /// * [`TooManyDescriptors`](Error::TooManyDescriptors)
    pub async fn set_raw(&mut self, s: &str) -> Result<(), Error> {
        let data = unsafe { extref::String::new(s) };
        component_method::<_, (), _>(
            self.invoker,
            self.buffer,
            &self.address,
            "setRaw",
            Some(&(data,)),
        )
        .await?;

        Ok(())
    }
}

pub trait HoloVal {
    fn get(&self) -> u8;
}

impl HoloVal for bool {
    fn get(&self) -> u8 {
        *self as u8
    }
}
impl HoloVal for u8 {
    fn get(&self) -> u8 {
        *self
    }
}
