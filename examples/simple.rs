#![no_main]

use std::convert::Infallible;

use oc_wasm_extras::hologram::{self, Hologram};
use oc_wasm_opencomputers::common::Lockable;
use oc_wasm_safe::component::{Invoker, Lister};
use oc_wasm_safe::computer;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

async fn main() -> Infallible {
    let _ = real_main().await;

    computer::shutdown();
}

async fn real_main() -> Result<()> {
    let mut invoker = Invoker::take().unwrap();
    let mut lister = Lister::take().unwrap();
    let mut buf = vec![];

    let mut holo = lister.start(Some(hologram::TYPE));
    let mut holo = Hologram::new(*holo.next().unwrap().address()).lock(&mut invoker, &mut buf);

    holo.clear().await?;
    let (w, _, d) = holo.get_dimensions().await?;
    for x in 1..=w {
        for z in 1..=d {
            holo.fill(x, z, None, 12, true).await?;
        }
    }

    Ok(())
}

#[no_mangle]
pub extern "C" fn run(arg: i32) -> i32 {
    oc_wasm_cassette::run(arg, main)
}
