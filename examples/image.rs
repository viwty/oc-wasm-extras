#![no_main]

use core::convert::Infallible;
use std::time::Duration;

use oc_wasm_extras::sewer::Sewer;
use oc_wasm_futures::sleep;
use oc_wasm_opencomputers::common::{Lockable, Point, Rgb};
use oc_wasm_opencomputers::gpu;
use oc_wasm_opencomputers::gpu::TextDirection::Horizontal;
use oc_wasm_safe::component::{Invoker, Lister};
use oc_wasm_safe::{computer, Address};
use zune_bmp::zune_core::colorspace::ColorSpace;
use zune_bmp::BmpDecoder;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

async fn main() -> Infallible {
    match real_main().await {
        Ok(_) => (),
        Err(e) => computer::error(&e.to_string()),
    };

    computer::shutdown();
}

fn get(lister: &mut Lister, n: &str) -> Address {
    let mut list = lister.start(None);
    let mut out = None;
    while let Some(a) = list.next() {
        if a.address().to_string().starts_with(n) {
            out = Some(*a.address());
        }
    }

    match out {
        Some(a) => a,
        None => computer::error(&format!("{n} not found")),
    }
}

async fn real_main() -> Result<()> {
    let mut invoker = Invoker::take().unwrap();
    let mut lister = Lister::take().unwrap();
    let mut buf = vec![];

    let mut gpu = lister.start(Some(gpu::TYPE));
    let gpu = gpu::Gpu::new(*gpu.next().unwrap().address());

    let mut decoder = BmpDecoder::new(include_bytes!("image.bmp"));
    let pixels = decoder.decode().unwrap();
    let (w, _h) = decoder.get_dimensions().unwrap();
    let colorspace = decoder.get_colorspace().unwrap();

    let data: Vec<_> = pixels
        .chunks(colorspace.num_components())
        .map(|rgb| {
            let (r, g, b) = match colorspace {
                ColorSpace::RGB => (rgb[2], rgb[1], rgb[0]),
                ColorSpace::RGBA => (rgb[0], rgb[1], rgb[2]),
                _ => computer::error(&format!("{colorspace:?}")),
            };

            ((r as u32) << 16) | ((g as u32) << 8) | b as u32
        })
        .collect();
    let data: Vec<&[_]> = data.chunks(w).collect();

    let mut gpu = gpu.lock(&mut invoker, &mut buf);
    let res = gpu.get_resolution().await?;

    for x in 1..=160 {
        for y in 1..=50 {
            let Some(color) = data.get(y - 1).map(|d| d.get(x - 1)).flatten() else {
                    break;
                };
            gpu.set_background(gpu::Colour::Rgb(Rgb(*color))).await?;

            gpu.set(
                Point {
                    x: x as u32,
                    y: y as u32,
                },
                " ",
                Horizontal,
            )
            .await?;
        }
    }

    gpu.set_background(gpu::Colour::Rgb(Rgb(0x000000))).await?;

    gpu.set(
        Point {
            x: 1,
            y: res.height,
        },
        &format!("{res:?}, {colorspace:?}"),
        Horizontal,
    )
    .await?;

    sleep::for_uptime(Duration::from_secs(3600)).await;

    Ok(())
}

#[no_mangle]
pub extern "C" fn run(arg: i32) -> i32 {
    oc_wasm_cassette::run(arg, main)
}
