#![no_main]

use std::convert::Infallible;

use oc_wasm_extras::hologram::{HoloVal, Hologram};
use oc_wasm_opencomputers::common::Lockable;
use oc_wasm_safe::component::{Invoker, Lister};
use oc_wasm_safe::{computer, Address};

fn formula(x: f64, z: f64) -> f64 {
    (x * z).cos()
}

struct Mult {
    pub holos: Vec<Hologram>,
}

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

impl Mult {
    async fn set(
        &self,
        inv: &mut Invoker,
        mut buf: &mut [u8],
        x: i32,
        y: i32,
        z: i32,
        value: impl HoloVal,
    ) -> Result<()> {
        let ix = (x - 1) / 48;
        let iz = (z - 1) / 48;
        self.holos[(ix + (iz * 4)) as usize]
            .lock(inv, &mut buf)
            .set(x - ix * 48, y, z - iz * 48, value)
            .await?;
        Ok(())
    }
    async fn get_dimensions(&self) -> Result<(i32, i32, i32)> {
        let len = (self.holos.len() as f64).sqrt() as i32;
        Ok((48 * len, 32, 48 * len))
    }
    async fn clear(&mut self, inv: &mut Invoker, mut buf: &mut [u8]) -> Result<()> {
        for h in self.holos.iter() {
            let mut holo = h.lock(inv, &mut buf);
            holo.clear().await?;
            holo.set_scale(1.0).await?;
        }

        Ok(())
    }
    async fn set_translation(
        &mut self,
        inv: &mut Invoker,
        mut buf: &mut [u8],
        tx: i32,
        ty: i32,
        tz: i32,
    ) -> Result<()> {
        for h in self.holos.iter() {
            let mut holo = h.lock(inv, &mut buf);

            holo.set_translation(tx, ty, tz).await?;
        }
        Ok(())
    }
}
async fn main() -> Infallible {
    let _ = real_main().await;

    computer::shutdown();
}

async fn real_main() -> Result<()> {
    let mut invoker = Invoker::take().unwrap();
    let mut lister = Lister::take().unwrap();
    let mut buf = [0; 512];

    // change me to false, too
    if true {
        computer::error("Change the hologram addresses in the example.");
    }

    let holos: Vec<Hologram> = vec![
        "3cba", "7cfa", "2ed5", "0616", "819b", "3368", "dee5", "6fb4", "84a2", "b532", "ab65",
        "d382", "2182", "379b", "104f", "3cb6",
    ]
    .iter()
    .map(|a| get(&mut lister, a))
    .map(|a| Hologram::new(a))
    .collect();

    let mut holo = Mult { holos };

    holo.clear(&mut invoker, &mut buf).await?;

    holo.set_translation(&mut invoker, &mut buf, 0, 1, 0)
        .await?;

    let (w, h, d) = holo.get_dimensions().await?;
    let (w, h, d) = (w, h, d);

    let scalar = 4.0;
    for x in (-w..=w).map(|x| (x as f64 / w as f64) * scalar) {
        for z in (-d..=d).map(|x| (x as f64 / d as f64) * scalar) {
            let y = formula(x, z);
            let x = ((w / 2) as f64 + (x / scalar) * (w as f64 / 2.0)).clamp(1.0, w as f64) as i32;
            let y = ((h / 2) as f64 + y * (h as f64 / 2.0)).clamp(1.0, h as f64) as i32;
            let z = ((d / 2) as f64 + (z / scalar) * (d as f64 / 2.0)).clamp(1.0, d as f64) as i32;
            holo.set(&mut invoker, &mut buf, x, y, z, 1).await?;
        }
    }

    Ok(())
}

fn get(lister: &mut Lister, t: &str) -> oc_wasm_safe::Address {
    let mut list = lister.start(None);
    let mut out: Option<Address> = None;
    while let Some(c) = list.next() {
        if c.address().to_string().starts_with(t) {
            out = Some(*c.address());
        }
    }
    match out {
        Some(c) => c,
        None => computer::error(t),
    }
}

#[no_mangle]
pub extern "C" fn run(arg: i32) -> i32 {
    oc_wasm_cassette::run(arg, main)
}
